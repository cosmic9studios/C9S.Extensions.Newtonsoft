using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace C9S.Extensions.Newtonsoft
{
    public static class JObjectExtensions
    {
        public static Dictionary<string, string> Flatten(this JObject jsonObject, string delimeter = null)
        {
            IEnumerable<JToken> jTokens = jsonObject.Descendants().Where(p => p.Count() == 0);
            Dictionary<string, string> results = jTokens.Aggregate(new Dictionary<string, string>(), (properties, jToken) =>
            {
                if (!string.IsNull(delimeter))
                {
                    properties.Add(jToken.Path.Replace(".", delimeter), jToken.ToString());
                }

                return properties;
            });

            return results;
        }
    }
}